import React from 'react';
import './App.css';
import Docs from './Components/Docs';
import { app, database } from './firebaseConfig';

function App() {
  return (
    <Docs database={database} />
  );
}

export default App;
