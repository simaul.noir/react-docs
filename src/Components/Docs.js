/* eslint-disable no-console */
/* eslint-disable react/prop-types */
/* eslint-disable no-alert */
import React, { useState, useEffect, useRef } from 'react';
import { addDoc, collection, onSnapshot } from 'firebase/firestore';
import DocsModal from './DocsModal';

export default function Docs({ database }) {
  const isMounted = useRef();
  const [docsData, setDocsData] = useState([]);
  const [title, setTitle] = useState('');
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const collectionRef = collection(database, 'docsData');

  const addData = () => {
    addDoc(collectionRef, {
      title,
    })
      .then(() => {
        setTitle('');
        handleClose();
      })
      .catch(() => {
        alert('Cannot add Data');
      });
  };

  const getData = () => {
    onSnapshot(collectionRef, (data) => {
      setDocsData(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    });
  };

  useEffect(() => {
    if (isMounted.current) {
      return;
    }

    isMounted.current = true;
    getData();
  }, []);

  return (
    <div className="docs-main">
      <h1>Docs Clone</h1>

      <button onClick={handleOpen} type="button" className="add-docs">
        Add a Document
      </button>

      <div className="grid-main">
        {docsData.map((doc) => (
          <div className="grid-child">
            <p>{doc.title}</p>
          </div>
        ))}
      </div>

      <DocsModal
        open={open}
        setOpen={setOpen}
        title={title}
        setTitle={setTitle}
        addData={addData}
      />
    </div>
  );
}
